DROP TABLE IF EXISTS 'csgrip';

CREATE TABLE 'csgrip' (
  'id' INTEGER PRIMARY KEY,
  'created' NUMERIC NOT NULL,
  'updated' NUMERIC NOT NULL,
  'location' TEXT NOT NULL,
  'description' TEXT NOT NULL
);

INSERT INTO 'csgrip' VALUES (21, datetime('now'), datetime('now'), 'Avans Lovensdijkstraat','CsGRIP frequency demo setup');

DROP TABLE IF EXISTS 'measurement';

CREATE TABLE 'measurement' (
  'id' INTEGER PRIMARY KEY,
  'frequency' NUMERIC NOT NULL,
  'm_id' INTEGER NOT NULL,
  FOREIGN KEY ('m_id') REFERENCES 'csgrip'('id') ON UPDATE CASCADE
);