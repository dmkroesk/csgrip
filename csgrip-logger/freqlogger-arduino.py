#!/usr/bin/python

import RPi.GPIO as GPIO
import signal
import time
import sqlite3 as lite
import logging
import sys
import os.path
import sys, getopt
import random
import serial

# Setup logger, using http://victorlin.me/posts/2012/08/26/good-logging-practice-in-python
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

cwd = os.path.dirname(__file__)
logfile = os.path.realpath("{0}/freqlogger.log".format(cwd))
handler = logging.FileHandler(logfile)
#handler.setLevel(logging.DEBUG)
handler.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

logger.addHandler(handler)

#
def ctrlc(sig, frame):
	global done 
	done = False


def main(argv):
	
	global done

	mid = 21

	try:
		opts, args = getopt.getopt(argv[1:],"hp:m:")
	except	getopt.GetoptError, err:
		logger.error(err)
		sys.exit(-1)
	
	for opt, arg in opts:
		if opt == "-h":
			print "Usage: {0} -h | -p<tty> -m<meter id>".format(argv[0])
			sys.exit()
		if opt == "-m":
			mid = int(arg)

	logger.debug("Starting frequency monitoring for id: {0}".format(mid))
	
	port = serial.Serial("/dev/ttyACM0", baudrate=9600, timeout=3.0)

	done = True;
	while done != False:

		port.write("g\n");
		rcv = port.readline().rstrip("\n\r ")

		try: 
			freq = 1000000.0 / float(long(rcv,10)) 
			
			try:
				cwd = os.path.dirname(__file__)
			 	dbfile = os.path.realpath("{0}/../database/freq.db".format(cwd))

			 	db = lite.connect(dbfile);
			 	cur = db.cursor();

			 	query = "INSERT INTO measurement (frequency, m_id) VALUES (%s, %s)" % (freq, mid)
			 	cur.execute(query)
			 	#logger.debug(query)

			 	db.commit()		

			except lite.Error, err:
			 	logger.error("%s" % err.args[0]);

			finally:
			 	if db:
			 		db.close();	

		except ValueError:
			logger.error("ValueError")


		time.sleep(2)	
	

	logger.debug('Cleanup, bye')

if __name__ == "__main__":
	main(sys.argv)
